package com.example.victor.imageloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Binder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    ImageView image;
    final static String url = "https://pp.vk.me/c631829/v631829852/a119/bLLeJlJm5Zs.jpg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image = (ImageView) findViewById(R.id.iv_image);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageLoader loader = new ImageLoader(getApplicationContext());
                loader.execute(url);
            }
        });


    }


    class ImageLoader extends AsyncTask<String, Integer, Bitmap> {

        Context mContext;

        ImageLoader(Context context) {
            mContext = context;
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap bitamp = BitmapFactory.decodeResource(mContext.getResources(),R.drawable.not_load);
            String urlText = strings[0];
            try {
                URL url = new URL(urlText);
                InputStream in = url.openStream();
                bitamp = BitmapFactory.decodeStream(in);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return bitamp;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            image.setImageBitmap(bitmap);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }
    }

}
